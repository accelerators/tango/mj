## Build

```bash
mkdir build
cd build
cmake ..
make
make install
```

## Examples

##### List some devices:
```bash
$ mj device list srdiag/ccdbpm/
srdiag/ccdbpm/bm08
srdiag/ccdbpm/bm16
srdiag/ccdbpm/bm20
srdiag/ccdbpm/bm21
srdiag/ccdbpm/bm22
srdiag/ccdbpm/bm28
srdiag/ccdbpm/d09
srdiag/ccdbpm/d17
srdiag/ccdbpm/d27
srdiag/ccdbpm/id04-2
srdiag/ccdbpm/id07
srdiag/ccdbpm/id25
```

##### List device attributes:
```bash
$ mj attribute list device srdiag/ccd/c27-1
ImageCounter ImageCounter ExternalTrigger FrameRate Exposure Gain Binning HorizontalFlip VerticalFlip AutoStart JpegImage JpegQuality JpegSmooth AcqErrorNumber Roi Image State Status
```

##### Read Attribute:
```bash
$ mj attribute read Exposure device srdiag/ccd/c27-1
37.8177
```

```bash
$ mj attribute read Roi format line device srdiag/ccd/c27-1
0 0 900 700
```

##### Redirect data to file:
```bash
$ mj attribute read Image format table device srdiag/ccd/c27-1 > last_image.data
```

##### Redirect to Gnuplot in terminal (need Sixel support):

![Data plotted through Sixel](img/sixel_plot.png "Sixel plot")

##### Event subscription
```bash
$ mj attribute event ImageCounter change device srdiag/ccd/c09-1
Subscribtion to event of type "CHANGE" of "ImageCounter" on "srdiag/ccd/c09-1"
Ctrl-C to stop
2678320
2678321
2678322
2678323
2678324
2678325
2678326
2678327
2678328
2678329
2678330
2678331
2678332
2678333
^C
Events received:   14
Duration:          0.9 s 
Average frequency: 15.7 Hz
```

