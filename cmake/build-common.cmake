# Avoid Re Load this module
set(MODULE_NAME build-common)
if(${MODULE_NAME})
    msg(DEBUG ">> module ${MODULE_NAME} already load")
    return()
else() 
    msg(DEBUG ">> Load module ${MODULE_NAME}")
    set(${MODULE_NAME} ON)
endif()

if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there." )
endif()

###############################################################################
# Load (if exists) cmake INSTITUTE configuration file with include macro with name:
#  - configProject()       -> Load after main configuration in this file
#  - configTangoClass()    -> Load after default Tango Class target configuration in config-classes.cmake
#  - configTangoServer()   -> Load after default Tango Server target configuration in config-servers.cmake
if(USER_CONFIG)
    includeOptional(${USER_CONFIG})
endif()

# Detect some system environment variables and if existing, recreate them as CMake variables
# to use them for config

setEnv2Cmake(CXX_STANDARD)
set(CMAKE_CXX_STANDARD ${CXX_STANDARD})

setEnv2Cmake(C_STANDARD)
set(CMAKE_C_STANDARD ${C_STANDARD})

# Check if execute from GitLab
if(DEFINED ENV{CI_COMMIT_SHA})
    set(ENV_GITLAB YES)
    set(ENV_INTERACTIVE NO)
else()
    set(ENV_GITLAB NO)
    set(ENV_INTERACTIVE YES)
endif()

set(PROJECT_ROOT_DIR        ${CMAKE_CURRENT_SOURCE_DIR})
set(PROJECT_CLASSES_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/classes)
set(PROJECT_SERVERS_DIR     ${CMAKE_CURRENT_SOURCE_DIR}/servers)
set(PROJECT_TEMPLATE_DIR    ${CMAKE_CURRENT_SOURCE_DIR}/cmake/template)

###############################################################################
# EXTERNAL/USER CONFIGURATION FOR GLOBAL PROJECT 
configProject()

# Get Git/Gitlab information to set variables
getGitInfo()

# Import Tango libraries Targets
find_package(Tango)

# test library (optional)
if(BUILD_TESTING)
  enable_testing()
  find_package(Test)
  add_custom_target(tests ALL)
endif()  

# Show all variable if debug/verbose
include(debug)
