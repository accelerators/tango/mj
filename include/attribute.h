#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include <span>
#include <string>
#include "mj_options.h"

int cmdAttribute(std::span<std::string> args, const mj::options &options);

#endif /* ATTRIBUTE_H */
