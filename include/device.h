#ifndef DEVICE_H
#define DEVICE_H

#include <span>
#include <string>
#include "mj_options.h"

int cmdDevice(std::span<std::string> args, const mj::options &options);

#endif /* DEVICE_H */
