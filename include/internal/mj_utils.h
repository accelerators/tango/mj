#ifndef MJ_UTILS_H
#define MJ_UTILS_H

#include <tango/tango.h>
#include "mj_options.h"

#include <tango_api/client/control_system.h>
#include <tango_api/client/device.h>
#include <tango_api/client/attribute.h>

namespace details
{
Tango::Database create_db(const mj::options &options);
tango::control_system create_control_system(const mj::options &options);
tango::device create_device(const std::string &name, const mj::options &options);
tango::attribute create_attribute(const std::string &name, const mj::options &options);
tango::attribute create_attribute(const std::string &dev_name, const std::string &name, const mj::options &options);
} // namespace details
#endif // MJ_UTILS_H
