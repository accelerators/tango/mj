#ifndef MJ_OPTIONS_H
#define MJ_OPTIONS_H

#include <string>

namespace mj
{
struct options
{
    bool env_tango_host;
    std::string tango_host;
    unsigned int port;
};
} // namespace mj
#endif // MJ_OPTIONS_H
