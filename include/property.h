#ifndef PROPERTY_H
#define PROPERTY_H

#include <span>
#include <string>
#include "mj_options.h"

int cmdProperty(std::span<std::string> args, const mj::options &options);

#endif /* PROPERTY_H */
