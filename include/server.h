#ifndef SERVER_H
#define SERVER_H

#include <span>
#include <string>
#include "mj_options.h"

int cmdServer(std::span<std::string> args, const mj::options &options);

#endif /* SERVER_H */
