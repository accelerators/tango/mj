#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include <vector>
#include <variant>
#include <string>
#include <ostream>
#include <memory>
#include <functional>

#include <tango_api/client/subscription.h>
#include <tango_api/common/quality.h>
#include <tango/tango.h>

namespace tango
{
typedef std::variant<bool,
                     std::vector<bool>,
                     std::vector<std::vector<bool>>,
                     float,
                     std::vector<float>,
                     std::vector<std::vector<float>>,
                     double,
                     std::vector<double>,
                     std::vector<std::vector<double>>,
                     std::int16_t,
                     std::vector<std::int16_t>,
                     std::vector<std::vector<std::int16_t>>,
                     std::uint16_t,
                     std::vector<std::uint16_t>,
                     std::vector<std::vector<std::uint16_t>>,
                     std::uint32_t,
                     std::vector<std::uint32_t>,
                     std::vector<std::vector<std::uint32_t>>,
                     std::int32_t,
                     std::vector<std::int32_t>,
                     std::vector<std::vector<std::int32_t>>,
                     std::int64_t,
                     std::vector<std::int64_t>,
                     std::vector<std::vector<std::int64_t>>,
                     std::uint64_t,
                     std::vector<std::uint64_t>,
                     std::vector<std::vector<std::uint64_t>>,
                     unsigned char,
                     std::vector<unsigned char>,
                     std::vector<std::vector<unsigned char>>,
                     std::string,
                     std::vector<std::string>,
                     std::vector<std::vector<std::string>>>
    attribute_value;
class event_data;

class attribute
{
    friend subscription::subscription(const attribute &,
                                      const Tango::EventType,
                                      std::function<void(const event_data &&)>);
    friend subscription::~subscription();

  public:
    attribute(const std::string &attr_name);
    attribute(const std::string &dev_name, const std::string &attr_name);
    attribute(const std::string &attr_name, const std::string host, unsigned int port);
    attribute(const std::string &dev_name, const std::string &attr_name, const std::string host, unsigned int port);

    std::string get_fqdn() const;

    const std::type_info &get_type() const;

    attribute_value get_value() const;
    quality get_quality() const;

    std::string get_name() const;
    std::string get_label() const;
    std::string get_description() const;

    subscription subscribe_event(Tango::EventType type, std::function<void(const event_data &&)> event);
    bool is_writable() const;
    bool is_polled() const;
    std::chrono::milliseconds get_polling_period();

  private:
    const std::string fqdn_name;
    std::unique_ptr<Tango::AttributeProxy> ptr_attr;
};

} // namespace tango
#endif // ATTRIBUTE_H
