#ifndef CONTROL_SYSTEM_H
#define CONTROL_SYSTEM_H

#include <memory>
#include <vector>
#include <string>

#include <tango_api/client/device.h>

#include <tango/tango.h>

namespace tango
{
class control_system
{
    friend class device;

  public:
    static const control_system &init_control_system();
    static const control_system &init_control_system(const std::string &host, unsigned int port);
    control_system();
    control_system(const std::string &host, unsigned int port);
    std::vector<std::string> get_servers() const;

    std::vector<device> get_devices() const;
    std::vector<device> get_devices(const std::string &domain_filter,
                                    const std::string &family_filter,
                                    const std::string &member_filter) const;

  private:
    std::unique_ptr<Tango::Database> db;
};
} // namespace tango

#endif /* CONTROL_SYSTEM_H */
