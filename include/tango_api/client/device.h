#ifndef DEVICE_H
#define DEVICE_H

#include <memory>
#include <vector>
#include <string>
#include <tango_api/client/property.h>
#include <tango_api/client/attribute.h>
#include <tango/tango.h>

namespace tango
{

class control_system;

class device
{
    friend std::ostream &operator<<(std::ostream &out, const device &p);

  public:
    device(const std::string &name);
    device(const std::string &name, const std::string &host, unsigned int port);
    device(const std::string &name, const control_system &parent_cs);

    std::string get_class() const;
    std::string get_full_name() const;
    std::string_view get_name() const;

    std::vector<property> get_properties(const std::vector<std::string> &filter) const;
    void write_property(const std::string &property_name, const std::string &value);
    void write_property(const std::string &property_name, const std::vector<std::string> &value);
    void write_property(const property &property);

    void delete_property(const std::string &property_name);
    void delete_property(const property &property);

    std::vector<attribute> get_attributes() const;

  private:
    std::string name;
    const control_system &cs;

    Tango::DbDevFullInfo get_info() const;
};
} // namespace tango

#endif // DEVICE_H
