#ifndef EVENT_DATA_H
#define EVENT_DATA_H

#include <variant>
#include <vector>
#include <cstdint>
#include <string>

#include "attribute.h"

namespace tango
{

class event_data
{
  public:
    const attribute &attr;
    const attribute_value value;
};
} // namespace tango
#endif
