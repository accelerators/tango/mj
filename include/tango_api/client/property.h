#ifndef PROPERTY_H
#define PROPERTY_H

#include <vector>
#include <string>
#include <ostream>

namespace tango
{

class property
{
  public:
    property(const std::string &property_name);

    property(const std::string &property_name, const std::vector<std::string> &property_value);

    const std::string &get_name() const;
    std::vector<std::string> get_value() const;

  private:
    std::string name;
    std::vector<std::string> value;
};

} // namespace tango

#endif // DEVICE_H
