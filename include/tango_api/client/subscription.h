#ifndef SUBSCRIPTION_H
#define SUBSCRIPTION_H

#include <memory>
#include <functional>

#include <tango/tango.h>

namespace tango
{
class attribute;
class event_data;

class subscription
{
  public:
    subscription(const attribute &attr, const Tango::EventType type, std::function<void(const event_data &&)> handler);
    ~subscription();

  private:
    const attribute &parent_attribute;
    int event_id;
    std::unique_ptr<Tango::CallBack> callback;
};
} // namespace tango
#endif
