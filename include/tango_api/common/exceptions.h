#ifndef TANGO_EXCEPTIONS_H
#define TANGO_EXCEPTIONS_H

#include <exception>
#include <stdexcept>

namespace tango
{
class attribute;

class host_not_found_exception : public std::runtime_error
{
  public:
    host_not_found_exception() :
        std::runtime_error{"Cannot connect to the control system. Is TANGO_HOST properly set ?"}
    {
    }
};

class device_not_found_exception : public std::runtime_error
{
  public:
    device_not_found_exception(const std::string &name) :
        std::runtime_error{"The device " + name + " was not found in the control system."}
    {
    }
};

class device_not_exported_exception : public std::runtime_error
{
  public:
    device_not_exported_exception(const std::string &name) :
        std::runtime_error{"Cannot connect to the device " + name + ". Is the server running ?"}
    {
    }
};

class attribute_exception : public std::runtime_error
{
  public:
    attribute_exception(const attribute &attr, const std::string &msg);

  private:
    const attribute &p_attr;
};

class attribute_not_found_exception : public attribute_exception
{
  public:
    attribute_not_found_exception(const attribute &attr);
};

class attribute_not_readable_exception : public attribute_exception
{
  public:
    attribute_not_readable_exception(const attribute &attr);
};

class bad_attribute_type_exception : public attribute_exception
{
  public:
    bad_attribute_type_exception(const attribute &attr, const std::type_info &real);
};

class unknown_format_exception : public attribute_exception
{
  public:
    unknown_format_exception(const attribute &attr);
};

class unsupported_type_exception : public attribute_exception
{
  public:
    unsupported_type_exception(const attribute &attr, const std::string &data_type);
};
} // namespace tango
#endif // TANGO_EXCEPTIONS_H
