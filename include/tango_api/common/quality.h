#ifndef QUALITY_H
#define QUALITY_H

#include <tango/tango.h>

namespace tango
{
typedef Tango::AttrQuality quality;
} // namespace tango
#endif
