#ifndef UTILS_H
#define UTILS_H
#include <unordered_map>
#include <typeinfo>
#include <typeindex>
#include <tango_api/client/attribute.h>
#include <tango_api/common/exceptions.h>
#include <tango/tango.h>

namespace tango
{
namespace details
{
template <class T>
struct extractor
{
    using type = std::vector<T>;
};

template <class T>
struct extractor<std::vector<T>>
{
    using type = std::vector<T>;
};

template <class T>
struct extractor<std::vector<std::vector<T>>>
{
    using type = std::vector<T>;
};

template <typename T>
using extractor_t = extractor<T>::type;

template <class T>
struct is_vector
{
    using type = T;
    using underlying_type = void;
    constexpr static bool value = false;
};

template <class T>
struct is_vector<std::vector<T>>
{
    using type = std::vector<T>;
    using underlying_type = T;
    constexpr static bool value = true;
};

template <typename T>
inline constexpr bool is_vector_v = is_vector<T>::value;

template <typename T>
using is_vector_t = typename is_vector<T>::type;

template <typename T>
using vector_underlying_t = typename is_vector<T>::underlying_type;

template <class T>
T extract_data(const attribute &attr, Tango::DeviceAttribute &dev_attr)
{
    details::extractor_t<T> extracted_data;
    dev_attr >> extracted_data;
    T data{};
    switch(dev_attr.get_data_format())
    {
    case Tango::SCALAR:
        if constexpr(!details::is_vector_v<T>)
        {
            data = extracted_data[0];
        }
        break;
    case Tango::SPECTRUM:
        if constexpr(std::is_same_v<T, details::extractor_t<T>>)
        {
            data = extracted_data;
        }
        break;
    case Tango::IMAGE:
    {
        if constexpr(details::is_vector_v<details::vector_underlying_t<T>>)
        {
            std::size_t xDim = dev_attr.get_dim_x();
            std::size_t yDim = dev_attr.get_dim_y();
            for(std::size_t i = 0; i != yDim; ++i)
            {
                details::vector_underlying_t<T> row;
                for(std::size_t j = 0; j != xDim; ++j)
                {
                    row.push_back(extracted_data[i * xDim + j]);
                }
                data.push_back(row);
            }
        }
    }
    break;
    default:
        throw unknown_format_exception(attr);
    }
    return data;
}

template <class T>
void extract_data_to_attr_value(const attribute &attr, Tango::DeviceAttribute &val, attribute_value &value)
{
    T data = extract_data<T>(attr, val);
    value = data;
}

static std::unordered_map<std::type_index,
                          std::function<void(const attribute &, Tango::DeviceAttribute &, attribute_value &)>>
    extract_functions{
        {std::type_index(typeid(bool)),                                    &extract_data_to_attr_value<bool>                      },
        {std::type_index(typeid(std::vector<bool>)),                       &extract_data_to_attr_value<std::vector<bool>>         },
        {std::type_index(typeid(std::vector<std::vector<bool>>)),
         &extract_data_to_attr_value<std::vector<std::vector<bool>>>                                                              },
        {std::type_index(typeid(unsigned char)),                           &extract_data_to_attr_value<unsigned char>             },
        {std::type_index(typeid(std::vector<unsigned char>)),              &extract_data_to_attr_value<std::vector<unsigned char>>},
        {std::type_index(typeid(std::vector<std::vector<unsigned char>>)),
         &extract_data_to_attr_value<std::vector<std::vector<unsigned char>>>                                                     },
        {std::type_index(typeid(float)),                                   &extract_data_to_attr_value<float>                     },
        {std::type_index(typeid(std::vector<float>)),                      &extract_data_to_attr_value<std::vector<float>>        },
        {std::type_index(typeid(std::vector<std::vector<float>>)),
         &extract_data_to_attr_value<std::vector<std::vector<float>>>                                                             },
        {std::type_index(typeid(double)),                                  &extract_data_to_attr_value<double>                    },
        {std::type_index(typeid(std::vector<double>)),                     &extract_data_to_attr_value<std::vector<double>>       },
        {std::type_index(typeid(std::vector<std::vector<double>>)),
         &extract_data_to_attr_value<std::vector<std::vector<double>>>                                                            },
        {std::type_index(typeid(std::int16_t)),                            &extract_data_to_attr_value<std::int16_t>              },
        {std::type_index(typeid(std::vector<std::int16_t>)),               &extract_data_to_attr_value<std::vector<std::int16_t>> },
        {std::type_index(typeid(std::vector<std::vector<std::int16_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::int16_t>>>                                                      },
        {std::type_index(typeid(std::uint16_t)),                           &extract_data_to_attr_value<std::uint16_t>             },
        {std::type_index(typeid(std::vector<std::uint16_t>)),              &extract_data_to_attr_value<std::vector<std::uint16_t>>},
        {std::type_index(typeid(std::vector<std::vector<std::uint16_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::uint16_t>>>                                                     },
        {std::type_index(typeid(std::uint32_t)),                           &extract_data_to_attr_value<std::uint32_t>             },
        {std::type_index(typeid(std::vector<std::uint32_t>)),              &extract_data_to_attr_value<std::vector<std::uint32_t>>},
        {std::type_index(typeid(std::vector<std::vector<std::uint32_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::uint32_t>>>                                                     },
        {std::type_index(typeid(std::int32_t)),                            &extract_data_to_attr_value<std::int32_t>              },
        {std::type_index(typeid(std::vector<std::int32_t>)),               &extract_data_to_attr_value<std::vector<std::int32_t>> },
        {std::type_index(typeid(std::vector<std::vector<std::int32_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::int32_t>>>                                                      },
        {std::type_index(typeid(std::int64_t)),                            &extract_data_to_attr_value<std::int64_t>              },
        {std::type_index(typeid(std::vector<std::int64_t>)),               &extract_data_to_attr_value<std::vector<std::int64_t>> },
        {std::type_index(typeid(std::vector<std::vector<std::int64_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::int64_t>>>                                                      },
        {std::type_index(typeid(std::uint64_t)),                           &extract_data_to_attr_value<std::uint64_t>             },
        {std::type_index(typeid(std::vector<std::uint64_t>)),              &extract_data_to_attr_value<std::vector<std::uint64_t>>},
        {std::type_index(typeid(std::vector<std::vector<std::uint64_t>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::uint64_t>>>                                                     },
        {std::type_index(typeid(std::string)),                             &extract_data_to_attr_value<std::string>               },
        {std::type_index(typeid(std::vector<std::string>)),                &extract_data_to_attr_value<std::vector<std::string>>  },
        {std::type_index(typeid(std::vector<std::vector<std::string>>)),
         &extract_data_to_attr_value<std::vector<std::vector<std::string>>>                                                       },
};
} // namespace details

} // namespace tango
#endif
