#include <chrono>
#include <iomanip>
#include <iostream>
#include <map>
#include <unordered_map>
#include <signal.h>
#include <tuple>
#include <type_traits>
#include <typeindex>
#include <variant>
#include <span>

#include <internal/mj_utils.h>
#include <tango/tango.h>
#include <attribute.h>
#include <tango_api/client/event_data.h>

enum Format
{
    LINE,
    COLUMN,
    TABLE,
};

namespace details
{

template <class T>
struct is_vector
{
    using type = T;
    using underlying_type = void;
    constexpr static bool value = false;
};

template <class T>
struct is_vector<std::vector<T>>
{
    using type = std::vector<T>;
    using underlying_type = T;
    constexpr static bool value = true;
};

template <typename T>
inline constexpr bool is_vector_v = is_vector<T>::value;

template <typename T>
using is_vector_t = typename is_vector<T>::type;

template <typename T>
using vector_underlying_t = typename is_vector<T>::underlying_type;

template <typename T>
static void print_data(const T &data, Format format)
{
    char sepColumn = ' ';
    char sepLine = ' ';
    switch(format)
    {
    case LINE:
        sepColumn = ' ';
        sepLine = ' ';
        break;
    case COLUMN:
        sepColumn = '\n';
        sepLine = '\n';
        break;
    case TABLE:
        sepColumn = ' ';
        sepLine = '\n';
        break;
    }

    if constexpr(!is_vector_v<T>)
    {
        std::cout << data;
    }
    else
    {
        for(const auto &val : data)
        {
            if constexpr(!is_vector_v<vector_underlying_t<T>>)
            {
                if(val != data.back())
                {
                    std::cout << val << sepColumn;
                }
                else
                {
                    std::cout << val;
                }
            }
            else
            {
                for(const auto &v : val)
                {
                    if(v != val.back())
                    {
                        std::cout << v << sepColumn;
                    }
                    else
                    {
                        std::cout << v;
                    }
                }
                if(val != data.back())
                {
                    std::cout << sepLine;
                }
            }
        }
    }
    std::cout << std::endl;
}

template <typename T>
static void print_attribute(const tango::attribute &attribute, Format format)
{
    std::visit([&](auto &&arg) { details::print_data(arg, format); }, attribute.get_value());
}

} // namespace details

static std::map<Tango::DevState, std::string> STATE_NAMES = {
    {Tango::ON,      "ON"     },
    {Tango::OFF,     "OFF"    },
    {Tango::CLOSE,   "CLOSE"  },
    {Tango::OPEN,    "OPEN"   },
    {Tango::INSERT,  "INSERT" },
    {Tango::EXTRACT, "EXTRACT"},
    {Tango::MOVING,  "MOVING" },
    {Tango::STANDBY, "STANDBY"},
    {Tango::FAULT,   "FAULT"  },
    {Tango::INIT,    "INIT"   },
    {Tango::RUNNING, "RUNNING"},
    {Tango::ALARM,   "ALARM"  },
    {Tango::DISABLE, "DISABLE"},
    {Tango::UNKNOWN, "UNKNOWN"},
};

static std::map<Tango::AttrQuality, std::string> QUALITY_NAMES = {
    {Tango::ATTR_VALID,    "VALID"   },
    {Tango::ATTR_INVALID,  "INVALID" },
    {Tango::ATTR_ALARM,    "ALARM"   },
    {Tango::ATTR_CHANGING, "CHANGING"},
    {Tango::ATTR_WARNING,  "WARNING" },
};

static std::map<Tango::EventType, std::string> EVENT_NAMES = {
    {Tango::CHANGE_EVENT,           "CHANGE"        },
    {Tango::QUALITY_EVENT,          "QUALITY"       },
    {Tango::PERIODIC_EVENT,         "PERIODIC"      },
    {Tango::ARCHIVE_EVENT,          "ARCHIVE"       },
    {Tango::USER_EVENT,             "USER"          },
    {Tango::ATTR_CONF_EVENT,        "ATTRIBUTE CONF"},
    {Tango::DATA_READY_EVENT,       "DATA READY"    },
    {Tango::INTERFACE_CHANGE_EVENT, ""              },
    {Tango::PIPE_EVENT,             "PIPE"          },
};

static void print_attribute(const tango::event_data &attribute, Format format)
{
    std::visit([&](auto &&arg) { details::print_data(arg, format); }, attribute.value);
}

static void print_attribute(const tango::attribute &attribute, Format format)
{
    std::visit([&](auto &&arg) { details::print_data(arg, format); }, attribute.get_value());
}

// Help command
static void usage(void)
{
    std::cerr << "usage: mj attribute list device DEV_NAME\n"
              << "       mj attribute info ATTR_NAME [device DEV_NAME]\n"
              << "       mj attribute read ATTR_NAME [format FORMAT] [device DEV_NAME]\n"
              << "       mj attribute event ATTR_NAME EVENT_TYPE [device DEV_NAME]\n"
              << std::endl
              << "ATTR_NAME  := attribute name\n"
              << "FORMAT     := {column|line|table}\n"
              << "EVENT_TYPE := {change|periodic|archive}\n"
              << "DEV_NAME   := tango device name" << std::endl;
}

// List attribute command
// Accept the following syntax
// list dev <DEV_NAME>
static int listAttribute(std::span<std::string> args, const mj::options &options)
{
    if(args.size() != 3)
    {
        usage();
        return EXIT_FAILURE;
    }

    std::string deviceName;
    if(std::string{"device"}.starts_with(args[1]))
    {
        deviceName = args[2];
    }
    else
    {
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        tango::device dev = details::create_device(deviceName, options);
        auto attributes = dev.get_attributes();
        for(const auto &attribute : attributes)
        {
            std::cout << attribute.get_name() << std::endl;
        }
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// Info attribute command
// Accept the following syntax
// info <ATT_NAME> dev <DEV_NAME>
static int infoAttribute(std::span<std::string> args, const mj::options &options)
{
    if(args.size() != 4 && args.size() != 2)
    {
        usage();
        return EXIT_FAILURE;
    }

    auto attName = args[1];
    std::string deviceName{""};
    if(args.size() == 4)
    {
        if(std::string{"device"}.starts_with(args[2]))
        {
            deviceName = args[3];
        }
        else
        {
            usage();
            return EXIT_FAILURE;
        }
    }

    try
    {
        auto attribute = details::create_attribute(deviceName, attName, options);
        std::cout << "Name           " << attribute.get_name() << '\n'
                  << "Label          " << attribute.get_label() << '\n'
                  << "Description    " << attribute.get_description() << '\n'
                  << "Writable       " << (attribute.is_writable() ? "Yes" : "No") << '\n'
                  << "Polling        " << (attribute.is_polled() ? "Yes" : "No") << " ("
                  << attribute.get_polling_period().count() << "ms)" << '\n'
                  << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// Read attribute command
// Accept the following syntax:
// read <ATT_NAME> dev <DEV_NAME>
// read <ATT_NAME> format <FORMAT> dev <DEV_NAME>
static int readAttribute(std::span<std::string> args, const mj::options &options)
{
    if(args.size() != 6 && args.size() != 4 && args.size() != 2)
    {
        usage();
        return EXIT_FAILURE;
    }

    auto attName = args[1];
    Format format = LINE;
    std::string deviceName{""};

    auto argc = args.size();
    switch(argc)
    {
    case 2:
        break;
    case 4:
        if(std::string{"device"}.starts_with(args[2]))
        {
            deviceName = args[3];
        }
        else
        {
            usage();
            return EXIT_FAILURE;
        }
        break;
    case 6:
        if(std::string{"format"}.starts_with(args[2]))
        {
            auto formatName = args[3];
            if(std::string{"line"}.starts_with(formatName))
            {
                format = LINE;
            }
            else if(std::string{"column"}.starts_with(formatName))
            {
                format = COLUMN;
            }
            else if(std::string{"table"}.starts_with(formatName))
            {
                format = TABLE;
            }
            else
            {
                usage();
                return EXIT_FAILURE;
            }
        }
        else
        {
            usage();
            return EXIT_FAILURE;
        }
        if(std::string{"device"}.starts_with(args[4]))
        {
            deviceName = args[5];
        }
        else
        {
            usage();
            return EXIT_FAILURE;
        }
        break;
    default:
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        auto attribute = details::create_attribute(deviceName, attName, options);
        print_attribute(attribute, format);

        const auto &quality = attribute.get_quality();
        if(quality != Tango::ATTR_VALID)
        {
            std::cerr << "Attribute quality is not valid: " << quality << std::endl;
        }
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// Event attribute command
// Accept the following syntax:
// event <ATT_NAME> <EV_TYPE> dev <DEV_NAME>
int eventAttribute(std::span<std::string> args, const mj::options &options)
{
    if(args.size() != 5 && args.size() != 3)
    {
        usage();
        return EXIT_FAILURE;
    }

    auto attName = args[1];

    // Tango::EventType evType;
    // auto event = args[2];
    static std::unordered_map<std::string, Tango::EventType> str_to_event{
        {"c",        Tango::CHANGE_EVENT  },
        {"ch",       Tango::CHANGE_EVENT  },
        {"cha",      Tango::CHANGE_EVENT  },
        {"chan",     Tango::CHANGE_EVENT  },
        {"chang",    Tango::CHANGE_EVENT  },
        {"change",   Tango::CHANGE_EVENT  },
        {"p",        Tango::PERIODIC_EVENT},
        {"pe",       Tango::PERIODIC_EVENT},
        {"per",      Tango::PERIODIC_EVENT},
        {"peri",     Tango::PERIODIC_EVENT},
        {"perio",    Tango::PERIODIC_EVENT},
        {"period",   Tango::PERIODIC_EVENT},
        {"periodi",  Tango::PERIODIC_EVENT},
        {"periodic", Tango::PERIODIC_EVENT},
        {"a",        Tango::ARCHIVE_EVENT },
        {"ar",       Tango::ARCHIVE_EVENT },
        {"arc",      Tango::ARCHIVE_EVENT },
        {"arch",     Tango::ARCHIVE_EVENT },
        {"archi",    Tango::ARCHIVE_EVENT },
        {"archiv",   Tango::ARCHIVE_EVENT },
        {"archive",  Tango::ARCHIVE_EVENT }
    };
    Tango::EventType evType;
    try
    {
        evType = str_to_event.at(args[2]);
    }
    catch(std::out_of_range &)
    {
        usage();
        return EXIT_FAILURE;
    }

    std::string deviceName{""};
    if(args.size() == 5)
    {
        if(std::string{"device"}.starts_with(args[3]))
        {
            deviceName = args[4];
        }
        else
        {
            usage();
            return EXIT_FAILURE;
        }
    }

    // connect to device
    try
    {
        auto attribute = details::create_attribute(deviceName, attName, options);
        // event subscription
        std::cerr << "Subscription to event of type \"" << EVENT_NAMES[evType] << "\" "
                  << "of \"" << attribute.get_fqdn() << "\" " << std::endl;
        std::cerr << "Ctrl-C to stop" << std::endl;

        auto t0 = std::chrono::steady_clock::now();
        std::size_t events_count = 0;
        auto sub = attribute.subscribe_event(evType,
                                             [&](const tango::event_data &&data) -> void
                                             {
                                                 ++events_count;
                                                 print_attribute(data, LINE);
                                             });

        sigset_t signalSet;
        int sig;
        sigemptyset(&signalSet);
        sigaddset(&signalSet, SIGINT);
        sigwait(&signalSet, &sig);
        // print summary
        auto t1 = std::chrono::steady_clock::now();
        auto d = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        std::cerr << std::fixed << std::setprecision(1) << "\n"
                  << "Events received:   " << events_count << "\n"
                  << "Duration:          " << d.count() / 1000.0 << " s \n"
                  << "Average frequency: " << events_count / (d.count() / 1000.0) << " Hz" << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

// Entry point
int cmdAttribute(std::span<std::string> args, const mj::options &options)
{
    if(args.size() > 1)
    {
        auto command = args[1];
        if(std::string{"list"}.starts_with(command))
        {
            return listAttribute(args.last(args.size() - 1), options);
        }
        if(std::string{"info"}.starts_with(command))
        {
            return infoAttribute(args.last(args.size() - 1), options);
        }
        if(std::string{"read"}.starts_with(command))
        {
            return readAttribute(args.last(args.size() - 1), options);
        }
        if(std::string{"event"}.starts_with(command))
        {
            return eventAttribute(args.last(args.size() - 1), options);
        }
        if(std::string{"help"}.starts_with(command))
        {
            usage();
            return EXIT_SUCCESS;
        }
    }
    usage();
    return EXIT_FAILURE;
}
