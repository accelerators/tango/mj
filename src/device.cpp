#include <cstdlib>
#include <array>
#include <span>

#include <internal/mj_utils.h>

#include <device.h>

static void usage(void)
{
    std::cerr << "usage: mj device list [DEV_PATH]\n"
              << "       mj device info DEV_NAME\n"
              << std::endl
              << "DEV_PATH := path to tango device, can be truncated\n"
              << "DEV_NAME := tango device name" << std::endl;
}

static int deviceList(std::span<std::string> args, const mj::options &options)
{
    try
    {
        auto cs = details::create_control_system(options);

        std::vector<tango::device> devices;
        switch(args.size())
        {
        case 1:
        {
            devices = cs.get_devices();
            break;
        }
        case 2:
        {
            std::istringstream ss;
            ss.str(args[1]);
            std::array<std::string, 3> words{"*", "*", "*"};
            std::size_t i = 0;
            for(std::string word; std::getline(ss, word, '/'); ++i)
            {
                words[i] = word;
            }

            const auto &[domain, family, member] = words;

            devices = cs.get_devices(domain, family, member);
            break;
        }
        default:
            usage();
            return EXIT_FAILURE;
        }

        for(auto &device : devices)
        {
            std::cout << device.get_name() << std::endl;
        }

        return EXIT_SUCCESS;
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}

static int deviceInfo(std::span<std::string> args, const mj::options &options)
{
    if(args.size() != 2)
    {
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        tango::device dev = details::create_device(args[1], options);

        std::cout << "Class name    " << dev.get_class() << std::endl;
        std::cout << "DS full name  " << dev.get_full_name() << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int cmdDevice(std::span<std::string> args, const mj::options &options)
{
    if(args.size() > 1)
    {
        auto cmd = args[1];
        if(std::string{"list"}.starts_with(cmd))
        {
            return deviceList(args.last(args.size() - 1), options);
        }
        if(std::string{"info"}.starts_with(cmd))
        {
            return deviceInfo(args.last(args.size() - 1), options);
        }
    }
    usage();
    return EXIT_FAILURE;
}
