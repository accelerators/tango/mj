#include <array>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iostream>
#include <vector>
#include <span>

#include <stdlib.h>
#include <argp.h>

#include "mj_options.h"
#include "attribute.h"
#include "device.h"
#include "property.h"
#include "server.h"

typedef std::function<int(std::span<std::string>, const mj::options &)> Function;
typedef std::pair<std::string, Function> Cmd;

// clang-format off
static const std::array<Cmd, 4> cmds = {{
     {"device", cmdDevice},
     {"property", cmdProperty},
     {"attribute", cmdAttribute},
     {"server", cmdServer},
}};
// clang-format on

/* Program documentation. */
static char doc[] = "mj -- a full tango command line client";

/* A description of the arguments we accept. */
static char args_doc[] = "device attribute property";

/* The options we understand. */
static const struct argp_option MJ_OPTIONS[] = {
    {"tango-host", 'h', "TANGO_HOST", 0, "TANGO_HOST to connect to", 0},
    {0,            0,   0,            0, 0,                          0}
};

/* Used by main to communicate with parse_opt. */
struct arguments
{
    mj::options options;
    std::vector<std::string> argv;
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    struct arguments *arguments = static_cast<struct arguments *>(state->input);

    switch(key)
    {
    case 'h':
    {
        arguments->options.env_tango_host = false;
        std::string host = arg;
        auto split = host.find(":");
        if(split != std::string::npos)
        {
            arguments->options.tango_host = host.substr(0, split);
            arguments->options.port = std::stoul(host.substr(split + 1));
        }
        else
        {
            arguments->options.tango_host = host;
            arguments->options.port = 10000u;
        }
    }
    break;

    case ARGP_KEY_ARG:
        arguments->argv.emplace_back(arg);
        break;

    case ARGP_KEY_END:
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

/* Our argp parser. */
static struct argp argp = {MJ_OPTIONS, parse_opt, args_doc, doc, nullptr, nullptr, nullptr};

static int doCmd(std::span<std::string> args, const mj::options &options)
{
    auto &command = args.front();

    for(auto &c : cmds)
    {
        if(c.first.starts_with(command))
        {
            return c.second(args, options);
        }
    }

    std::cerr << "Command \"" << command << "\"not found, check help" << std::endl;
    return EXIT_FAILURE;
}

static void usage(void) { }

int main(int argc, char **argv)
{
    struct arguments arguments;

    /* Default values. */
    arguments.options.env_tango_host = true;

    /* Parse our arguments; every option seen by parse_opt will
       be reflected in arguments. */
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if(!arguments.argv.empty())
    {
        std::span<std::string> args{arguments.argv};
        return doCmd(args, arguments.options);
    }
    else
    {
        usage();
        return EXIT_SUCCESS;
    }
}
