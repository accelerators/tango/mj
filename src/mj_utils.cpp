#include <internal/mj_utils.h>

namespace details
{
Tango::Database create_db(const mj::options &options)
{
    if(options.env_tango_host)
    {
        return Tango::Database();
    }
    else
    {
        return Tango::Database(options.tango_host, options.port);
    }
}

tango::control_system create_control_system(const mj::options &options)
{
    if(options.env_tango_host)
    {
        return tango::control_system();
    }
    else
    {
        return tango::control_system(options.tango_host, options.port);
    }
}

tango::device create_device(const std::string &name, const mj::options &options)
{
    if(options.env_tango_host)
    {
        return tango::device(name);
    }
    else
    {
        return tango::device(name, options.tango_host, options.port);
    }
}

tango::attribute create_attribute(const std::string &name, const mj::options &options)
{
    if(options.env_tango_host)
    {
        return tango::attribute(name);
    }
    else
    {
        return tango::attribute(name, options.tango_host, options.port);
    }
}

tango::attribute create_attribute(const std::string &dev_name, const std::string &name, const mj::options &options)
{
    return create_attribute(dev_name + (dev_name.empty() ? "" : "/") + name, options);
}
} // namespace details
