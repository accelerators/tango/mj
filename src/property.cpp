#include <iostream>
#include <span>

#include <internal/mj_utils.h>
#include <tango_api/client/device.h>
#include <tango_api/client/property.h>

#include <property.h>

static void usage(void)
{
    std::cerr << "usage: mj property read [PROP_NAME] device DEV_NAME\n"
              << "       mj property write PROP_NAME VALUE device DEV_NAME\n"
              << "       mj property delete PROP_NAME device DEV_NAME\n"
              << std::endl
              << "PROP_NAME := property name\n"
              << "VALUE     := property value\n"
              << "DEV_NAME  := tango device name" << std::endl;
}

static int readProperty(const std::span<std::string> &args, const mj::options &options)
{
    // read <PROP_NAME> device <DEV_NAME>
    // read device <DEV_NAME>

    if(args.size() < 3)
    {
        usage();
        return EXIT_FAILURE;
    }

    std::string deviceName;
    std::vector<std::string> propertyNames;
    if(args.size() == 4 && std::string{"device"}.starts_with(args[2]))
    {
        propertyNames.push_back(args[1]);
        deviceName = args[3];
    }
    else if(args.size() == 3 && std::string{"device"}.starts_with(args[1]))
    {
        deviceName = args[2];
    }
    else
    {
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        tango::device dev = details::create_device(deviceName, options);
        auto properties = dev.get_properties(propertyNames);
        for(const auto &property : properties)
        {
            auto values = property.get_value();
            if(!values.empty())
            {
                std::cout << property.get_name() << " " << values[0] << std::endl;
                for(const auto &v : std::span(values.begin() + 1, values.size() - 1))
                {
                    std::cout << " "
                              << " ";
                    std::cout << v << std::endl;
                }
            }
        }
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int writeProperty(std::span<std::string> args, const mj::options &options)
{
    // write <PROP_NAME> <VALUE> device <DEV_NAME>
    // write <PROP_NAME> device <DEV_NAME>

    std::string deviceName;
    std::string propName;
    std::string propValue;
    auto argc = args.size();
    switch(argc)
    {
    case 4:
        if(std::string{"device"}.starts_with(args[2]))
        {
            deviceName = args[3];
            propName = args[1];
            break;
        }
        usage();
        return EXIT_FAILURE;
    case 5:
        if(std::string{"device"}.starts_with(args[3]))
        {
            propName = args[1];
            propValue = args[2];
            deviceName = args[4];
            break;
        }
        usage();
        return EXIT_FAILURE;
    default:
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        tango::device dev = details::create_device(deviceName, options);

        dev.write_property(propName, propValue);
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

static int deleteProperty(std::span<std::string> args, const mj::options &options)
{
    // delete <PROP_NAME> device <DEV_NAME>

    if(args.size() < 4)
    {
        usage();
        return EXIT_FAILURE;
    }

    auto propName = args[1];
    std::string deviceName;
    if(std::string{"device"}.starts_with(args[2]))
    {
        deviceName = args[3];
    }
    else
    {
        usage();
        return EXIT_FAILURE;
    }

    try
    {
        tango::device dev = details::create_device(deviceName, options);

        dev.delete_property(propName);
    }
    catch(std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int cmdProperty(std::span<std::string> args, const mj::options &options)
{
    if(args.size() > 1)
    {
        auto &cmd = args[1];
        if(std::string{"read"}.starts_with(cmd))
        {
            return readProperty(args.last(args.size() - 1), options);
        }
        if(std::string{"write"}.starts_with(cmd))
        {
            return writeProperty(args.last(args.size() - 1), options);
        }
        if(std::string{"delete"}.starts_with(cmd))
        {
            return deleteProperty(args.last(args.size() - 1), options);
        }
    }
    usage();
    return EXIT_FAILURE;
}
