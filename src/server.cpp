#include <iostream>
#include <cstring>

#include <tango_api/client/control_system.h>
#include <tango_api/common/exceptions.h>

#include <internal/mj_utils.h>

#include <server.h>

static void usage(void)
{
    std::cerr << "usage: mj server list" << std::endl;
}

static int serverList(std::span<std::string> args, const mj::options &options)
{
    try
    {
        auto cs = details::create_control_system(options);

        switch(args.size())
        {
        case 1:
        {
            for(auto &dev : cs.get_servers())
            {
                std::cout << dev << std::endl;
            }
            return EXIT_SUCCESS;
        }
        default:
            usage();
            return EXIT_FAILURE;
        }
    }
    catch(const tango::host_not_found_exception &e)
    {
        std::cerr << "Could not connect to the control system : " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}

int cmdServer(std::span<std::string> args, const mj::options &options)
{
    if(args.size() == 2)
    {
        if(std::string{"list"}.starts_with(args[1]))
        {
            return serverList(args.last(args.size() - 1), options);
        }
    }
    usage();
    return EXIT_FAILURE;
}
