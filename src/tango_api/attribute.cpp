#include <tango_api/client/attribute.h>
#include <tango_api/common/exceptions.h>
#include <tango_api/internal/utils.h>
#include <sstream>
#include <tango/tango.h>

namespace tango
{
namespace details
{
std::string attribute_fqdn(const std::string &attr_name, const std::string &host, unsigned int port)
{
    std::stringstream fqdn;
    fqdn << "tango://" << host << ":" << port << "/" << attr_name;
    return fqdn.str();
}

template <class T>
const std::type_info &get_type_info(Tango::AttrDataFormat format, const attribute &attr)
{
    switch(format)
    {
    case Tango::SCALAR:
        return typeid(T);
    case Tango::SPECTRUM:
        return typeid(std::vector<T>);
    case Tango::IMAGE:
        return typeid(std::vector<std::vector<T>>);
    default:
        throw unknown_format_exception(attr);
    }
}

template <class T>
T get_value(const attribute &attr, Tango::AttributeProxy &proxy)
{
    if(attr.get_type() != typeid(T))
    {
        throw bad_attribute_type_exception(attr, typeid(T));
    }
    Tango::DeviceAttribute dev_attr = proxy.read();

    return tango::details::extract_data<T>(attr, dev_attr);
}
} // namespace details

attribute::attribute(const std::string &attr_name) :
    fqdn_name(attr_name)
{
    try
    {
        ptr_attr = std::make_unique<Tango::AttributeProxy>(fqdn_name.c_str());
    }
    catch(Tango::DevFailed &)
    {
        throw attribute_not_found_exception(*this);
    }
}

attribute::attribute(const std::string &dev_name, const std::string &attr_name) :
    attribute(dev_name + "/" + attr_name)
{
}

attribute::attribute(const std::string &attr_name, const std::string host, unsigned int port) :
    attribute(details::attribute_fqdn(attr_name, host, port))
{
}

attribute::attribute(const std::string &dev_name,
                     const std::string &attr_name,
                     const std::string host,
                     unsigned int port) :
    attribute(dev_name + "/" + attr_name, host, port)
{
}

std::string attribute::get_fqdn() const
{
    return fqdn_name;
}

std::string attribute::get_name() const
{
    if(ptr_attr)
    {
        try
        {
            auto attributeInfo = ptr_attr->get_config();
            return attributeInfo.name;
        }
        catch(Tango::DevFailed &)
        {
            throw attribute_not_readable_exception(*this);
        }
    }
    else
    {
        return fqdn_name;
    }
}

std::string attribute::get_label() const
{
    try
    {
        auto attributeInfo = ptr_attr->get_config();
        return attributeInfo.label;
    }
    catch(Tango::DevFailed &)
    {
        throw attribute_not_readable_exception(*this);
    }
}

std::string attribute::get_description() const
{
    try
    {
        auto attributeInfo = ptr_attr->get_config();
        return attributeInfo.description;
    }
    catch(Tango::DevFailed &)
    {
        throw attribute_not_readable_exception(*this);
    }
}

bool attribute::is_writable() const
{
    try
    {
        auto attributeInfo = ptr_attr->get_config();
        return attributeInfo.writable;
    }
    catch(Tango::DevFailed &)
    {
        throw attribute_not_readable_exception(*this);
    }
}

bool attribute::is_polled() const
{
    try
    {
        return ptr_attr->is_polled();
    }
    catch(Tango::DevFailed &)
    {
        throw attribute_not_readable_exception(*this);
    }
}

std::chrono::milliseconds attribute::get_polling_period()
{
    return std::chrono::milliseconds{ptr_attr->get_poll_period()};
}

attribute_value attribute::get_value() const
{
    Tango::DeviceAttribute dev_attr = ptr_attr->read();
    attribute_value value;
    auto &function = details::extract_functions.at(std::type_index(get_type()));
    function(*this, dev_attr, value);
    return value;
}

quality attribute::get_quality() const
{
    return ptr_attr->read().get_quality();
}

const std::type_info &attribute::get_type() const
{
    auto dev_attr = ptr_attr->read();
    auto format = dev_attr.get_data_format();
    switch(dev_attr.get_type())
    {
    case Tango::DEV_BOOLEAN:
        return details::get_type_info<bool>(format, *this);
    case Tango::DEV_UCHAR:
        return details::get_type_info<unsigned char>(format, *this);
    case Tango::DEV_DOUBLE:
        return details::get_type_info<double>(format, *this);
    case Tango::DEV_FLOAT:
        return details::get_type_info<float>(format, *this);
    case Tango::DEV_SHORT:
        return details::get_type_info<std::int16_t>(format, *this);
    case Tango::DEV_USHORT:
        return details::get_type_info<std::uint16_t>(format, *this);
    case Tango::DEV_LONG:
        return details::get_type_info<std::int32_t>(format, *this);
    case Tango::DEV_ULONG:
        return details::get_type_info<std::uint32_t>(format, *this);
    case Tango::DEV_LONG64:
        return details::get_type_info<std::int64_t>(format, *this);
    case Tango::DEV_ULONG64:
        return details::get_type_info<std::uint64_t>(format, *this);
    case Tango::DEV_STRING:
        return details::get_type_info<std::string>(format, *this);
    default:
    {
        std::stringstream type;
        type << dev_attr.get_type();
        throw unsupported_type_exception(*this, type.str());
    }
    }
}

subscription attribute::subscribe_event(Tango::EventType type, std::function<void(const event_data &&)> handler)
{
    return subscription(*this, type, handler);
}
} // namespace tango
