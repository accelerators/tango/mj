#include <tango_api/client/control_system.h>
#include <tango_api/common/exceptions.h>

#include <type_traits>
#include <regex>
#include <array>
#include <functional>

namespace tango
{

namespace details
{

std::vector<std::string>
    query_db_for_devices(Tango::Database &db,
                         const std::vector<std::string> &prefixes,
                         std::string_view filter,
                         std::function<Tango::DbDatum(Tango::Database &, const std::string &)> function)
{
    std::vector<std::string> ret;
    std::vector<std::string> l_prefixes = prefixes;
    if(l_prefixes.empty())
    {
        l_prefixes.push_back("");
    }
    for(const auto &prefix : l_prefixes)
    {
        std::vector<std::string> db_infos;
        std::string query = prefix.empty() ? "*" : prefix + "/*";
        std::string l_prefix = prefix.empty() ? "" : (prefix + "/");
        Tango::DbDatum res = function(db, query);
        res >> db_infos;
        std::string regex_str = std::string(filter);
        auto star_pos = regex_str.find("*");
        if(star_pos != std::string::npos)
        {
            regex_str.replace(star_pos, 1, ".*");
        }
        std::regex reg(regex_str);
        for(const auto &db_info : db_infos)
        {
            if(std::regex_match(db_info, reg))
            {
                std::string current_dev = l_prefix + db_info;
                ret.push_back(current_dev);
            }
        }
    }
    return ret;
}

template <class T>
auto get_devices_impl(T &db,
                      std::vector<std::string> &ret,
                      const std::string &domain_filter,
                      const std::string &family_filter,
                      const std::string &member_filter,
                      int) -> decltype(db.get_device_defined(""))
{
    std::string query = domain_filter + "/" + family_filter + "/" + member_filter;
    Tango::DbDatum res = db.get_device_defined(query);
    res >> ret;
    return res;
}

template <class T>
auto get_devices_impl(T &db,
                      std::vector<std::string> &ret,
                      const std::string &domain_filter,
                      const std::string &family_filter,
                      const std::string &member_filter,
                      double) -> void
{
    std::array<std::tuple<std::string_view, std::function<Tango::DbDatum(Tango::Database &, const std::string &)>>, 3>
        functions{std::make_tuple(domain_filter, &Tango::Database::get_device_domain),
                  std::make_tuple(family_filter, &Tango::Database::get_device_family),
                  std::make_tuple(member_filter, &Tango::Database::get_device_member)};
    for(const auto &function : functions)
    {
        ret = query_db_for_devices(db, ret, std::get<0>(function), std::get<1>(function));
    }
}

std::vector<std::string> get_devices_impl(Tango::Database &db,
                                          const std::string &domain_filter,
                                          const std::string &family_filter,
                                          const std::string &member_filter)
{
    std::vector<std::string> devices;
    get_devices_impl(db, devices, domain_filter, family_filter, member_filter, 0);
    return devices;
}

static std::map<std::pair<std::string, unsigned int>, control_system> control_systems;
} // namespace details

const control_system &control_system::init_control_system()
{
    return init_control_system("", 0);
}

const control_system &control_system::init_control_system(const std::string &host, unsigned int port)
{
    auto key = std::make_pair(host, port);
    if(host.empty())
    {
        details::control_systems.try_emplace(key);
    }
    else
    {
        details::control_systems.try_emplace(key, host, port);
    }
    return details::control_systems[key];
}

control_system::control_system()
{
    try
    {
        db = std::make_unique<Tango::Database>();
    }
    catch(Tango::ConnectionFailed &e)
    {
        throw host_not_found_exception{};
    }
}

control_system::control_system(const std::string &host, unsigned int port)
{
    try
    {
        db = std::make_unique<Tango::Database>(host, port);
    }
    catch(Tango::ConnectionFailed &e)
    {
        throw host_not_found_exception{};
    }
}

std::vector<std::string> control_system::get_servers() const
{
    std::vector<std::string> servers;

    Tango::DbDatum res;
    if(db)
    {
        res = db->get_server_name_list();
    }

    res >> servers;

    return servers;
}

std::vector<device> control_system::get_devices() const
{
    return get_devices("*", "*", "*");
}

std::vector<device> control_system::get_devices(const std::string &domain_filter,
                                                const std::string &family_filter,
                                                const std::string &member_filter) const
{
    auto devices = details::get_devices_impl(*db, domain_filter, family_filter, member_filter);

    std::vector<device> ret;
    for(const auto &d : devices)
    {
        ret.emplace_back(d, *this);
    }
    return ret;
}
} // namespace tango
