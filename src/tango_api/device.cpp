#include <tango_api/client/device.h>
#include <tango_api/client/control_system.h>
#include <tango_api/common/exceptions.h>

#include <tango/tango.h>

namespace tango
{
namespace details
{
std::string device_fqdn(const std::string &dev_name, const std::string &host, unsigned int port)
{
    std::stringstream fqdn;
    fqdn << "tango://" << host << ":" << port << "/" << dev_name;
    return fqdn.str();
}
} // namespace details

device::device(const std::string &dev_name) :
    name(dev_name),
    cs(control_system::init_control_system())
{
}

device::device(const std::string &dev_name, const control_system &parent_cs) :
    name(dev_name),
    cs(parent_cs)
{
}

device::device(const std::string &dev_name, const std::string &host, unsigned int port) :
    name(dev_name),
    cs(control_system::init_control_system(host, port))
{
}

std::string device::get_class() const
{
    Tango::DbDevFullInfo res = get_info();

    return res.class_name;
}

std::string device::get_full_name() const
{
    Tango::DbDevFullInfo res = get_info();

    return res.ds_full_name;
}

Tango::DbDevFullInfo device::get_info() const
{
    try
    {
        return cs.db->get_device_info(name);
    }
    catch(Tango::DevFailed &e)
    {
        throw device_not_found_exception{name};
    }
}

std::string_view device::get_name() const
{
    return name;
}

std::vector<property> device::get_properties(const std::vector<std::string> &filter) const
{
    std::vector<property> properties;

    Tango::DbDatum res;

    // read property names
    std::vector<std::string> propertyNames;
    try
    {
        if(filter.empty())
        {
            res = cs.db->get_device_property_list(name, "*");
            res >> propertyNames;
        }
        else
        {
            propertyNames = filter;
        }
    }
    catch(Tango::DevFailed &e)
    {
        throw device_not_found_exception{name};
    }

    // escape if there are no properties
    if(propertyNames.empty())
    {
        return properties;
    }

    // read property values
    Tango::DbData dbData;
    try
    {
        cs.db->get_device_property(name, dbData);
    }
    catch(Tango::DevFailed &e)
    {
        throw device_not_found_exception{name};
    }

    // format output
    for(size_t i = 0; i < propertyNames.size(); i++)
    {
        std::vector<std::string> value;
        dbData[i] >> value;

        properties.emplace_back(propertyNames[i], value);
    }

    return properties;
}

void device::write_property(const std::string &property_name, const std::string &value)
{
    std::vector<std::string> values;
    values.push_back(value);
    write_property(property_name, values);
}

void device::write_property(const std::string &property_name, const std::vector<std::string> &value)
{
    property prop{property_name, value};
    write_property(prop);
}

void device::write_property(const property &property)
{
    Tango::DbDatum prop(property.get_name());
    prop << property.get_value();

    Tango::DbData dbData;
    dbData.push_back(prop);
    cs.db->put_device_property(name, dbData);
}

void device::delete_property(const std::string &property_name)
{
    property prop{property_name};
    delete_property(prop);
}

void device::delete_property(const property &property)
{
    Tango::DbDatum prop(property.get_name());
    Tango::DbData dbData;
    dbData.push_back(prop);

    cs.db->delete_device_property(name, dbData);
}

std::vector<attribute> device::get_attributes() const
{
    std::vector<std::string> attributes;
    Tango::DeviceProxy dev_proxy;
    try
    {
        dev_proxy = Tango::DeviceProxy(name);
    }
    catch(Tango::DevFailed &)
    {
        throw device_not_found_exception(name);
    }

    std::unique_ptr<std::vector<std::string>> attList;
    try
    {
        attList.reset(dev_proxy.get_attribute_list());
    }
    catch(Tango::DevFailed &)
    {
        throw device_not_exported_exception(name);
    }

    std::vector<attribute> res;
    for(const auto &att_name : *attList.get())
    {
        res.emplace_back(name + "/" + att_name);
    }
    return res;
}
} // namespace tango
