#include <tango_api/common/exceptions.h>
#include <tango_api/client/attribute.h>

namespace tango
{
attribute_exception::attribute_exception(const attribute &attr, const std::string &msg) :
    std::runtime_error(msg),
    p_attr(attr)
{
}

attribute_not_found_exception::attribute_not_found_exception(const attribute &attr) :
    attribute_exception{attr,
                        "Cannot find attribute " + attr.get_name() +
                            ". Maybe the device is not exported, or the attribute does not exist."}
{
}

attribute_not_readable_exception::attribute_not_readable_exception(const attribute &attr) :
    attribute_exception{attr, "Cannot read attribute " + attr.get_name()}
{
}

bad_attribute_type_exception::bad_attribute_type_exception(const attribute &attr, const std::type_info &real) :
    attribute_exception{attr,
                        "Trying to read attribute with type " + std::string(attr.get_type().name()) +
                            " while attribute is of type " + std::string(real.name())}
{
}

unknown_format_exception::unknown_format_exception(const attribute &attr) :
    attribute_exception{attr, "Attribute " + attr.get_name() + " has an unknown format."}
{
}

unsupported_type_exception::unsupported_type_exception(const attribute &attr, const std::string &data_type) :
    attribute_exception{attr, "Attribute " + attr.get_name() + " is of unsupported data type:" + data_type}
{
}
} // namespace tango
