#include <tango_api/client/property.h>
#include <tango_api/common/exceptions.h>
#include <span>

namespace tango
{

property::property(const std::string &property_name) :
    property(property_name, std::vector<std::string>{})
{
}

property::property(const std::string &property_name, const std::vector<std::string> &property_value) :
    name(property_name),
    value(property_value)
{
}

const std::string &property::get_name() const
{
    return name;
}

std::vector<std::string> property::get_value() const
{
    return value;
}
} // namespace tango
