#include <tango_api/common/quality.h>
#include <tango/tango.h>

namespace details
{
static std::unordered_map<tango::quality, std::string> QUALITY_NAMES = {
    {Tango::ATTR_VALID,    "VALID"   },
    {Tango::ATTR_INVALID,  "INVALID" },
    {Tango::ATTR_ALARM,    "ALARM"   },
    {Tango::ATTR_CHANGING, "CHANGING"},
    {Tango::ATTR_WARNING,  "WARNING" },
};
}

[[maybe_unused]] static std::ostream &operator<<(std::ostream &os, const tango::quality &quality)
{
    os << details::QUALITY_NAMES[quality];
    return os;
}
