#include <unordered_map>
#include <typeindex>

#include <tango_api/client/subscription.h>
#include <tango_api/client/attribute.h>
#include <tango_api/client/event_data.h>
#include <tango_api/internal/utils.h>

#include <tango/tango.h>

namespace tango
{
namespace details
{

event_data build_event_data(const attribute &attr, Tango::DeviceAttribute &val)
{
    attribute_value value;
    auto &function = extract_functions.at(std::type_index(attr.get_type()));
    function(attr, val, value);
    return event_data{attr, value};
}
} // namespace details

subscription::subscription(const attribute &attr,
                           const Tango::EventType type,
                           std::function<void(const event_data &&)> handler) :
    parent_attribute(attr)
{
    // event callback
    class local_callback : public Tango::CallBack
    {
      private:
        std::function<void(const event_data &&)> local_handler;
        const attribute &attr;

      public:
        local_callback(const attribute &attribute, std::function<void(const event_data &&)> handler) :
            local_handler(handler),
            attr(attribute)
        {
        }

        void push_event(Tango::EventData *ed) override
        {
            auto attr_value = ed->attr_value;
            local_handler(details::build_event_data(attr, *attr_value));
        }
    };

    callback = std::make_unique<local_callback>(parent_attribute, handler);
    try
    {
        event_id = parent_attribute.ptr_attr->subscribe_event(type, callback.get());
    }
    catch(Tango::DevFailed &e)
    {
        std::cerr << "Unable to subscribe to event:" << e << std::endl;
    }
}

subscription::~subscription()
{
    parent_attribute.ptr_attr->unsubscribe_event(event_id);
}
} // namespace tango
