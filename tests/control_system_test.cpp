#include "mj_test.h"

#include <tango_api/client/control_system.h>
#include <tango_api/common/exceptions.h>

namespace details
{

auto unset_env(const std::string &var) -> int
{
#ifdef _TG_WINDOWS_
    return _putenv_s(var.c_str(), "");
#else
    return unsetenv(var.c_str());
#endif
}

} // namespace details

namespace tests
{
BOOST_AUTO_TEST_SUITE(control_system)

BOOST_AUTO_TEST_CASE(throw_on_missing_TANGO_HOST)
{
    details::unset_env("TANGO_HOST");

    BOOST_CHECK_THROW(new tango::control_system(), tango::host_not_found_exception);
}

BOOST_AUTO_TEST_SUITE_END()
} // namespace tests
